package org.alioune.tpxml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.dom4j.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSReader {
	public static InputStream read(String url) throws UnsupportedOperationException, IOException {
		//�ouverture�d'un�client� �
		HttpClient httpClient=HttpClientBuilder.create().build();
		//�cr�ation�d'une�m�thode�HTTP� HttpGet�
		HttpGet get=new HttpGet(url);
		HttpResponse response =httpClient.execute(get);
		StatusLine statusLine=response.getStatusLine();
		//�invocation�de�la�m�thode
		int statusCode=statusLine.getStatusCode();
		if(statusCode !=HttpStatus.SC_OK){
			System.err.println("Methode failed: "+statusLine);
		}
		//�lecture�de�la�r�ponse�dans�un�flux� InputStream�
		InputStream inputStream=response.getEntity().getContent();
		return inputStream;
		
	}
	public static void main(String... args) throws ParserConfigurationException, SAXException {
		String url="https://www.voxxed.com/feed/";
		try {
			InputStream inputStream=read(url);
			SAXParserFactory sp=SAXParserFactory.newInstance();
			sp.setNamespaceAware(true);
			sp.setValidating(true);
			SAXParser parser=sp.newSAXParser();
			Element racine=null;
			//question 2
			final class CountingHandler extends DefaultHandler{
				private int count;
				public void startElement (String uri, String localName,
	                    String qName, Attributes attributes) {
			    	count++;
			    }
				public int getCount() {
					return count;
				}
			}
			CountingHandler dh1=new CountingHandler();
			parser.parse(inputStream, dh1);
			 int compteur=dh1.getCount()-1;
			 System.out.println("Nombre de sous-�l�ment: "+compteur);
			DefaultHandler dh=new DefaultHandler() {
		    public void startDocument() {
		    	System.out.println("D�but du document");
		    }
		    public void startElement (String uri, String localName,
                    String qName, Attributes attributes) {
		    	if(localName.equals("rss")) {
		    		System.out.println(localName);
		    	}
		    }
			};
			 inputStream=read(url);
			parser.parse(inputStream, dh);
			
		
			 final class CountingItemHandler extends DefaultHandler{
					private int count;
					private String item;
					boolean test=false;
					public CountingItemHandler(String item) {
						this.item=item;
					}
					public void startElement (String uri, String localName,
		                    String qName, Attributes attributes) {
						if(localName.equals(item)) {
							test=true;
							if(test) 
								count++;
						}    
				    }
					 public void endElement (String uri, String localName, String qName)
						    {
						        if(localName.equals(item))
						        	test=false;
						    }
					public int getCount() {
						return count;
					}
				}
			// question 4
			 inputStream=read(url);
			 CountingItemHandler dh2=new CountingItemHandler("item"); //g�n�rique
			 parser.parse(inputStream, dh2);
			 System.out.println("item: "+dh2.getCount());
			 //question 5
			 final class TitleHandler extends DefaultHandler{
				private boolean test;
				private String buffer;
				private List<String> list=new ArrayList<String>();
				@Override
				public void startElement(String uri, String localName, String qName, Attributes attributes) {
					// TODO Auto-generated method stub
					
						if(localName.equals("item")) {
							test=true;
						}
				}
				@Override
				public void characters(char[] ch, int start, int length) throws SAXException {
					// TODO Auto-generated method stub
					if(test) {
						buffer=new String(ch,start, length);
					}
				}
				@Override
				public void endElement(String uri, String localName, String qName) throws SAXException {
					// TODO Auto-generated method stub
						if(localName.equals("item")) {
							test=false;
						}
						if(qName.equals("title")) {
							list.add(buffer);
						}
				}
				public List<String> getList() {
					return list;
				} 
			 }
			 inputStream=read(url);
			 TitleHandler dh3=new TitleHandler(); 
			 parser.parse(inputStream, dh3);
			 for(String title: dh3.getList()) {
				 System.out.println(title+"\n");
			 }
			 //question 6
			 final class CategoryHandler extends DefaultHandler{
					private boolean test;
					private String buffer;
					private List<String> list=new ArrayList<String>();
					@Override
					public void startElement(String uri, String localName, String qName, Attributes attributes) {
						// TODO Auto-generated method stub
						
							if(localName.equals("category")) {
								test=true;
							}
					}
					@Override
					public void characters(char[] ch, int start, int length) throws SAXException {
						// TODO Auto-generated method stub
						if(test) {
							buffer=new String(ch,start, length);
						}		
					}
					@Override
					public void endElement(String uri, String localName, String qName) throws SAXException {
						// TODO Auto-generated method stub
							if(localName.equals("category")) {
								test=false;
								list.add(buffer);
							}	
					}
					public List<String> getList() {
						return list;
					}
				 }
			 inputStream=read(url);
			 CategoryHandler dh4=new CategoryHandler(); 
			 parser.parse(inputStream, dh4);
			 System.out.println("-----------------------------------");
			 for(String title: dh4.getList()) {
				 System.out.println(title+"\n");
			 }
			 System.out.println("-----------------------------------");
			 //question 7
			 final class EspaceHandler extends DefaultHandler{
					private boolean test;
					private int compteur;
					 public void startElement (String uri, String localName,
                             String qName, Attributes attributes){
						 if(uri.equals("http://purl.org/rss/1.0/modules/slash/")) {
							 compteur++;
						 }
                     }
					public int getCompteur() {
						return compteur;
					}
				 }
			 inputStream=read(url);
			 EspaceHandler dh5=new EspaceHandler(); 
			 parser.parse(inputStream, dh5);
			 System.out.println(" nombre d��l�ments attach�s � l�espace de nom:  "+dh5.getCompteur());
			 //question 8
			 final class MicroservicesHandler extends DefaultHandler{
					private int compteur;
					private boolean test;
					private String buffer;
					private String title;
					private List<String> list=new ArrayList<String>();
					@Override
					public void startElement(String uri, String localName, String qName, Attributes attributes) {
						// TODO Auto-generated method stub
						
							if(localName.equals("item")) {
								test=true;
							}
					}
					@Override
					public void characters(char[] ch, int start, int length) throws SAXException {
						// TODO Auto-generated method stub
						if(test) {
							buffer=new String(ch,start, length);
						}
					}
					@Override
					public void endElement(String uri, String localName, String qName) throws SAXException {
						// TODO Auto-generated method stub
						
							if(localName.equals("item")) {
								test=false;
							}
							if(qName.equals("title")) {
								title=buffer;
							}
								
							if(qName.equals("category")) {
								if(buffer.equals("microservices")) {
									list.add(title);
								}
								
							}
					}
					public List<String> getList() {
						return list;
					} 
					
				 }
			 inputStream=read(url);
			 MicroservicesHandler dh6=new MicroservicesHandler(); 
			 parser.parse(inputStream, dh6);
			 System.out.println("-----------------------------------");
			for(String titre: dh6.getList()) {
				System.out.println(" titre: "+titre);
			}
			 System.out.println("-----------------------------------");
			 
			 
			
			
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
