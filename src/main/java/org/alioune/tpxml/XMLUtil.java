package org.alioune.tpxml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.xml.sax.XMLReader;

public class XMLUtil {
	public static  Document serialize(Marin m) {
		Document doc=DocumentHelper.createDocument();
		Element racine=doc.addElement("Marin").addAttribute("id", m.getId()+"");
		racine.addElement("nom").addText(m.getNom());
		racine.addElement("prenom").addText(m.getPrenom());
		racine.addElement("age").addText(m.getAge()+"");
		return doc;
	}
	public static void write(Document doc, File file) throws IOException {
		OutputFormat format = OutputFormat.createPrettyPrint();
		Writer out=new FileWriter(file);
		XMLWriter writer=new XMLWriter(out,format);
		writer.write(doc);
		writer.flush();
		writer.close();
	}
	public static Document read(File file) throws DocumentException {
		SAXReader reader=new SAXReader();
		Document doc=reader.read(file);
		return doc;
	}
	public static Marin deserialize(Document doc) {
		Marin m=new Marin();
		Element racine = doc.getRootElement();
		String id=racine.attribute("id").getStringValue();
		if(racine.attribute("id")!=null){
			m.setId(Long.parseLong(id));
		}
		List<Element> elements=racine.elements();
		for(Element elem:elements) {
			switch(elem.getName()) {
			case "nom":
			{
				m.setNom(elem.getText());
				break;
			}
			case "prenom":
			{
				m.setPrenom(elem.getText());
				break;
			}
			case "age":
			{
				m.setAge(Integer.parseInt(elem.getText()));
				break;
			}
			}
		}
		
		return m;
	}
	public static void main(String...args) {
		File file=new File("xml/out.xml");
		Marin m=new Marin(12,"Surcouf","Robert",23);
	    Document doc=serialize(m);
		Marin m2=deserialize(doc);
			
		System.out.println(m2);
			try {
				write(doc, file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
