package org.alioune.tpxml;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
public class Amazon {
	public static List<String> listNode(XPath xpath, File fileXML) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document xml = builder.parse(fileXML);
	    Element root = xml.getDocumentElement();
		List<String> list=new ArrayList<String>();
		NodeList liste=(NodeList)xpath.evaluate("//types/schema/*",root,XPathConstants.NODESET);
		for(int i=0;i<liste.getLength();i++) {
			list.add(liste.item(i).getNodeName());
		}
		return list;
	}
	public static List<Node> listValueAttr(XPath xpath, File fileXML) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document xml = builder.parse(fileXML);
	    Element root = xml.getDocumentElement();
		List<Node> list=new ArrayList<Node>();
		NodeList liste=(NodeList)xpath.evaluate("//message[@name]/@name",root,XPathConstants.NODESET);
		for(int i=0;i<liste.getLength();i++) {
			list.add(liste.item(i));
		}
		return list;
	}
	public static void main(String... args) throws  XPathExpressionException, SAXException, IOException, ParserConfigurationException {
	     File fileXML = new File("xml/amazon.xml");
	     DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	     DocumentBuilder builder = factory.newDocumentBuilder();
	     Document xml = builder.parse(fileXML);
	     Element root = xml.getDocumentElement();
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		NodeList list=(NodeList)xpath.evaluate("//types/schema/*",root,XPathConstants.NODESET);
		System.out.println(list.getLength());// 85 sous-�l�ments
		List<String> names=listNode(xpath,fileXML);
		for(String name:names) {
			System.out.println(name);
		}
		System.out.println("------------------------------------------------------------");
		List<Node> valueAttr=listValueAttr(xpath,fileXML);
		for(Node node:valueAttr) {
			System.out.println(node);
		}
		
		
	}

}
